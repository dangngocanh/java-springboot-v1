package com.erp.anhdn.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    @GetMapping(value = "/demo", produces = "application/json")
    public ResponseEntity<String> demo(){
        return new ResponseEntity<String>("Hello World!", HttpStatus.OK);
    }

}
